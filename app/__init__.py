#!/usr/bin/env python3
# -*- coding: utf-8 i-*-

from flask import Flask, render_template, __version__


app = Flask(__name__)


@app.route("/")
def index():
    return render_template("index.html", version=__version__)

