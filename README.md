# Flask Quickstart

A Boilerplate for developing your own flask project

## Setup and Explanation

See [seminar-informatik-20.gitlab.io - flask quickstart](https://seminar-informatik-20.gitlab.io/articles/guides/flask-quickstart/).

## Execution

You can either use the flask cli tool:

`flask run`

Or start the app manually:

`python3 main.py`
